def binary_search(list, item):
    low = 0                 #low and high keep track of
    high = len(list) - 1    #which part of the list you search in

    while low <= high:      #while you havent narrowed it down to one element
        mid = (low + high)  #check the middle element
        guess = list[mid]
        if guess == item:   #if youve found the item
            return mid
        if guess > item:    #if the guess was too high
            high = mid - 1
        else:               #if the guess was too low
            low = mid + 1
    return None             #the item doesnt exist


my_list = [1, 3, 5, 7, 9]   #a test set of numbers

print(binary_search(my_list, 7))#since the list starts at index 0, the output should be 3 because
                                #7 is at index position 3

print(binary_search(my_list, -1))#we get an output of None here or "Null" because there isnt a -1 in the list